# Ansible: Automatisation d'un serveur maison + Test avec Molecule CI avec Gitlab

<!-- class: invert -->

Stack logiciel utilisé:

```
Molecule: 3.2.4
Python: 3.9
Ansible: 2.9.18
Podman: (Remplacement/concurent de Docker)
```

---

# Exposé de la problématique

Un serveur maison avec des applications non documenté,
Donc un risque d'un point de vu professionnel.
C'est un cas intéressant a prendre comme exemple
Il arrive souvent chez le client qu'un serveur ne soit pas documenté et/ou reinstallable.

---

# Environnement technique coté client

Un pc fixe sous Fedora 34 avec l'impossibilité d'installer docker du a des incompatibilité des Cgroups-V2.

---

# Environnement technique coté serveur

Un Serveur fedora 33 qui pourrais évoluer sur un Centos Stream ou une Debian 10.

## Avec les logiciels suivants

- Mono
- jellyfin
- Samba
- Raddar, Lidarr, Sonarr
- Abelujo et Abstock ( Dans un repo séparé)

---

# La solution choisie:

Utiliser l'outils ansible et tester les différents roles et playbook avec Molecule qui servira de test d'intégration continue.
Utiliser Git et gitlab et sa CI/CD qui nous permettra de faire les tests d'intégration, de syntaxe(linter), et qui vérifiera que l'application est bien à l'état voulu.

---

![bg right](https://ih1.redbubble.net/image.919251757.3389/flat,750x,075,f-pad,750x1000,f8f8f8.jpg)

**Ansible:** Outil de gestion de configuration et de provisionnement.

SSH pour se connecter aux serveurs

 Le projet a été fondé en 2013 et racheté par Red Hat en 2015.

## L'idempotence c'est quoi ?

Un élément clé est la question de l'**idempotence** d'Ansible.
C'est ce qui permet de ne pas se poser la question de l'état du serveur en face qu'il soit déjà installé mal installé ou pas installé.

---

**Molecule:** L'idée est d'aller vers de l'infra as code, on a un code fiable et on ne se préoccupe plus du reste ou presque :tada:.

# Pourquoi tester du code infra en tant qu'Administrateur système ?

- Le code doit être maintenu ( version d'ansible, CVE ect ...)
- Éviter les arrêts de production
- Pouvoir reproduire un serveur a l'identique présente beaucoup d'avantages ( Moins d'action manuelle, correction automatique, scalabilité, data recovery )
- Tester manuellement sa marche peu souvent ... :fire:

---

# Molecule et les conteneurs:

On va donc utiliser Molecule et Podman pour créer un conteneur qui va mimer une machine vierge, et on va tester les playbooks et les roles un par un dans notre CI ansible.

# Ansible 2.9 et les collections:

On va profiter pour mettre a jour avec le système de collections d'ansible dans notre requirements.yml.

# On va utiliser Gitlab CI pour créer a la volée des conteneurs et tester nos logiciels et notre code infra ansible.

---

# Démo/Utilisation:

Avec un environnement ansible/molecule/podman:

```bash
#On va dans le dossier/role que l'on veut tester
molecule test # Pour un test complet
molecule converge # Pour provisionner la VM/conteneur et jouer le playbook
molecule login # Pour se connecter au conteneur pour debuger
molecule verify # Pour executer les verifications
# Pour deployer sur une vrai machine on peut rajouter le --check pour faire un dry run
ansible-playbook playbooks/deploy_serveurmaison.yml -i inventory all -vvv  --diff
```

---

# Avec une vrai application en Django et Common lisp ça donne quoi :

Python 2, Django et common lisp : Abelujo et Abstock

---

# Demo/Utilisation sur un projet de abelujo-ansible librairie:

Avec un environnement ansible/molecule/podman:

```bash
#On va dans le dossier/role que l'on veut tester
cd abelujo
molecule converge
molecule verify
```

---

# Les améliorations possibles ?

Regarder en détails les problèmes d'idempotence des différents roles
Passer a des applications plus facilement automatisables.
Prendre un projet réel.

---

Ressources complémentaires:
https://www.jeffgeerling.com/blog/2018/testing-your-ansible-roles-molecule
https://github.com/geerlingguy/molecule-playbook-testing
